function hierarchicalView(whitehatProfiles, blackhatProfiles){
	function textFromStringOrArray(source){
		if (typeof source === "string"){
		    return source;
		}
		if(Array.isArray(source)){
		    var out = "";
		    for(k in source){
		        if(typeof source[k] !== "string"){
		            throw new Error("element of array was not string");
		        }
		        out += source[k];
		    }
		    return out;
		}
		throw new Error("source was not array of string or string");
	}

	function htmlOfProfile(obj,hat){
		var html = "<div class='profile'>";
		if(hat==="white"){		
			html += "\n<h3 class='whitehat'>"+obj.name+"</h3>";
		}
		else{
			html += "\n<h3 class='blackhat'>"+obj.name+"</h3>";
		}
		html += "\n<p>"+ textFromStringOrArray(obj.description) + "</p>";
		html += htmlOfWants(obj.wants);
		html += "\n</div>";

		return html;
	}

	function htmlOfWants(obj){
		var html = "<div class='want'>";
		html += "\n<h4>Wants...</h4>";
		html += "\n<ul class='ulwant'>";
		for(k in obj){
            const current = obj[k];
		    html += "\n   <li>" + textFromStringOrArray(current.text) + "</li>";
		    if(current.subwants.length!=0){
		        html += "\n   <ul class='subwants'>";
		        for(k2 in current.subwants){
                    html+= "\n      <li>"+current.subwants[k2]+"</li>"
		            //html+= "<li>"+textFromStringOrArray(obj[k].subwants[k2])+"</li>"
		        }
		        html +="\n   </ul>";
		    }
		}
		html += "\n</ul>"
		html += "</div>";
		return html;
	}

	var innerhtml = '';
    
	for(prop in whitehatProfiles){
		var pf = whitehatProfiles[prop];
		innerhtml += htmlOfProfile(pf,'white');	
	}
	for(prop in blackhatProfiles){
		var pf = blackhatProfiles[prop];
		innerhtml += htmlOfProfile(pf,'black');	
	}

	return innerhtml;
}
//var exports = {};
if(typeof exports !== 'undefined'){
exports.hierarchicalView = hierarchicalView
}
