//Script that will populate 
var whitehatProfiles = {}
var blackhatProfiles = {}

function loadProfile(filename,hat){


	fetch(filename)
		.then(function(response){
			return response.json()
		})
		.then(function(myJson){
			//console.log(myJson);
			const towrite = myJson.name;
			if(hat==="white"){
				whitehatProfiles[towrite] = myJson;
			}else{
				blackhatProfiles[towrite] = myJson;
			}
		})
		.catch(err => alert(filename+ ": " + err))
		.then(function(){
			document.getElementById('view').innerHTML = hierarchicalView(whitehatProfiles,blackhatProfiles);
		});
}

function loadProfileWhitehat(filename){
	loadProfile(filename,"white")
}

function loadProfileBlackhat(filename){
	loadProfile(filename,"black") 
}

loadProfileWhitehat("./whitehats/basicimuser.json");
loadProfileWhitehat("./whitehats/discorduser.json");
loadProfileWhitehat("./whitehats/forumuser.json");
loadProfileWhitehat("./whitehats/grandma.json");
loadProfileWhitehat("./whitehats/largecommunityadmin.json");
loadProfileWhitehat("./whitehats/organization.json");
loadProfileWhitehat("./whitehats/privacyfan.json");
loadProfileWhitehat("./whitehats/signaluser.json");
loadProfileWhitehat("./whitehats/skypeuser.json");
loadProfileWhitehat("./whitehats/teen.json");

loadProfileBlackhat("./blackhats/datahoarder.json");
loadProfileBlackhat("./blackhats/destroyer.json");
loadProfileBlackhat("./blackhats/identitythief.json");
loadProfileBlackhat("./blackhats/spammer.json");
loadProfileBlackhat("./blackhats/stalker.json");
loadProfileBlackhat("./blackhats/troll.json");


