### User stories

Project to define something akin to user stories in JSON format so that it can be displayed/formatted in
a flexible yet consistent way. Will also make it easier to filter and tag user stories later
on if the amount of user stories grows.

There are two folders that contain the user story personas. 
Whitehats (users we want to accomodate) and Blackhats (users we do _not_ want to accomodate)


There are two different JSON objects. Profile and Want

Profile:
* name : String
* description : Array of String, or String
* wants : Array of Want


Want:
* text : Array of String, or String
* subwants : Array of String

To contribute:
* add _wants_ elements to the profiles (in blackhats and whitehats folder)
* add new profiles (remember to add the json in the profileLoad.js script)
* write html/css that can display the data in a different or improved manner, e.g with filters and search
* write javascript that can transform the json-data into some other format, e.g for a spreadsheet

To use/view:
* clone repository, and open view.html in a browser