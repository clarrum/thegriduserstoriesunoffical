// ***
//
// nodejs script to generate a static html page.
//
// ***


const skeleton = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'> \
<title>Grid user stories, VIEW</title><link href='style1.css' rel='stylesheet' type='text/css' media='all'> \
<style>REPLACEMESTYLE</style> \
</head><body><h1>Grid user stories</h1><p>Static view generated with nodejs</p> \
<div id='view'>REPLACEMECONTENT</div></body></html>"



const fs = require('fs');
const displayHierarchical = require('../js/displayHierarchical.js');




var whitehats = []
const whitehatFiles = fs.readdirSync('../whitehats');
whitehatFiles.forEach(file => {
    var fileContents = fs.readFileSync('../whitehats/'+file);
    var data = JSON.parse(fileContents);
	whitehats.push(data);
})

var blackhats = []
const blackhatFiles = fs.readdirSync('../blackhats');
blackhatFiles.forEach(file => {
    var fileContents = fs.readFileSync('../blackhats/'+file);
    var data = JSON.parse(fileContents);
	blackhats.push(data);
})




const view = displayHierarchical.hierarchicalView(whitehats,blackhats);//.replace(/>/g,">\n");
const style = fs.readFileSync('../style1.css');

fs.writeFile("../viewStatic.html", skeleton.replace('REPLACEMECONTENT',view).replace('REPLACEMESTYLE',style), function(err) {
    if(err) {
        return console.log(err);
    }
}); 

